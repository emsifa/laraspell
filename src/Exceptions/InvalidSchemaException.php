<?php

namespace LaraSpell\Exceptions;

use UnexpectedValueException;

class InvalidSchemaException extends UnexpectedValueException 
{

}
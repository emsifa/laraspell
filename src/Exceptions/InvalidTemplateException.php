<?php

namespace LaraSpell\Exceptions;

use UnexpectedValueException;

class InvalidTemplateException extends UnexpectedValueException 
{
    
}

<?php

namespace LaraSpell;

class AssetCollector
{
    protected $externalStyles = [];
    protected $externalScripts = [];
    protected $externalStyleAliases = [];
    protected $externalScriptAliases = [];

    protected $internalStyles = [];
    protected $internalScripts = [];
    protected $internalStyleAliases = [];
    protected $internalScriptAliases = [];

    public function getExternalStyles()
    {
        return $this->externalStyles;
    }

    public function getExternalScripts()
    {
        return $this->externalScripts;
    }

    public function getInternalStyles()
    {
        return $this->internalStyles;
    }

    public function getInternalScripts()
    {
        return $this->internalScripts;
    }

    public function addExternalStyle($cssFile, $alias = null)
    {
        if (in_array($cssFile, $this->externalStyles) OR ($alias AND array_key_exists($alias, $this->externalStyleAliases))) {
            return;
        }

        $this->externalStyles[] = $cssFile;
        $index = count($this->externalStyles) - 1;
        if ($alias) {
            $this->externalStyleAliases[$alias] = $index;
        }
    }

    public function addExternalScript($jsFile, $alias = null)
    {
        if (in_array($jsFile, $this->externalScripts) OR ($alias AND array_key_exists($alias, $this->externalScriptAliases))) {
            return;
        }

        $this->externalScripts[] = $jsFile;
        $index = count($this->externalScripts) - 1;
        if ($alias) {
            $this->externalScriptAliases[$alias] = $index;
        }
    }

    public function addInternalStyle($style, $alias = null)
    {
        if ($alias AND array_key_exists($alias, $this->internalStyleAliases)) {
            return;
        }

        $this->internalStyles[] = $style;
        $index = count($this->internalStyles) - 1;
        if ($alias) {
            $this->internalStyleAliases[$alias] = $index;
        }
    }

    public function addInternalScript($script, $alias = null)
    {
        if ($alias AND array_key_exists($alias, $this->internalScriptAliases)) {
            return;
        }

        $this->internalScripts[] = $script;
        $index = count($this->internalScripts) - 1;
        if ($alias) {
            $this->internalScriptAliases[$alias] = $index;
        }
    }

    public function renderStyles()
    {
        $styles = "";
        $externalStyles = $this->getExternalStyles();
        $internalStyles = $this->getInternalStyles();
        
        foreach($externalStyles as $style) {
            $url = $this->isUrl($style)? $style : asset($style);
            $styles .= "<link rel='stylesheet' href='{$url}'/>";
        }
        foreach ($internalStyles as $style) {
            $styles .= $style;
        }
        
        return $styles;
    }

    public function renderScripts()
    {
        $scripts = "";
        $externalScripts = $this->getExternalScripts();
        $internalScripts = $this->getInternalScripts();

        foreach($externalScripts as $script) {
            $url = $this->isUrl($script)? $script : asset($script);
            $scripts .= "<script type='text/javascript' src='{$url}'></script>";
        }
        foreach ($internalScripts as $script) {
            $scripts .= $script;
        }
        
        return $scripts;
    }

    protected function isUrl($str)
    {
        return (bool) preg_match("/^((https?\:)?\/\/)/", $str);
    }

}
